# Network List

<a href="https://gitee.com/mindspore/docs/tree/master/docs/source_en/network_list.md" target="_blank"><img src="./_static/logo_source.png"></a>

## Model Zoo
|  Domain | Sub Domain    | Network                                   | Ascend | GPU | CPU 
|:------   |:------| :-----------                               |:------   |:------  |:-----
|Computer Version (CV) | Image Classification  | [AlexNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/alexnet/src/alexnet.py)          |  Supported |  Supported | Doing
| Computer Version (CV)  | Image Classification  | [GoogleNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/googlenet/src/googlenet.py)                                               |  Supported     | Doing | Doing
| Computer Version (CV)  | Image Classification  | [LeNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/lenet/src/lenet.py)              |  Supported |  Supported | Supported
| Computer Version (CV)  | Image Classification  | [ResNet-50](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/resnet/src/resnet.py)          |  Supported |  Doing | Doing
|Computer Version (CV)  | Image Classification  | [ResNet-101](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/resnet/src/resnet.py)                                              |  Supported |Doing | Doing
| Computer Version (CV)  | Image Classification  | [VGG16](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/vgg16/src/vgg.py)                |  Supported |  Doing | Doing
| Computer Version (CV)  | Mobile Image Classification<br>Image Classification<br>Semantic Tegmentation  | [MobileNetV2](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv2/src/mobilenetV2.py)                                            |  Supported |  Supported | Doing
| Computer Version (CV)  | Mobile Image Classification<br>Image Classification<br>Semantic Tegmentation  | [MobileNetV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv3/src/mobilenetV3.py)                                            |  Doing |  Supported | Doing
|Computer Version (CV)  | Targets Detection  | [SSD](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/ssd/src/ssd.py)                   |  Supported |Doing | Doing
| Computer Version (CV)  | Targets Detection  | [YoloV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/yolov3/src/yolov3.py)         |  Supported |  Doing | Doing
| Computer Version (CV)  | Targets Detection  | [FasterRCNN](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/faster_rcnn/src/FasterRcnn)         |  Supported |  Doing | Doing
| Computer Version (CV) | Semantic Segmentation  | [Deeplabv3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/deeplabv3/src/deeplabv3.py)                                          |  Supported |  Doing | Doing
| Natural Language Processing (NLP) | Natural Language Understanding  | [BERT](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/bert/src/bert_model.py)                                          |  Supported |  Doing | Doing
| Natural Language Processing (NLP) | Natural Language Understanding  | [Transformer](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/Transformer/src/transformer_model.py)                                          |  Supported |  Doing | Doing
| Natural Language Processing (NLP) | Natural Language Understanding  | [SentimentNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/lstm/src/lstm.py)                                          |  Doing |  Supported | Supported
| Recommender | Recommender System, CTR prediction  | [DeepFM](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/deepfm/src/deepfm.py)                                          |  Supported |  Doing | Doing
| Recommender | Recommender System, Search ranking  | [Wide&Deep](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/wide_and_deep/src/wide_and_deep.py)                                          |  Supported |  Doing | Doing


## Pre-trained Models
Coming soon.
