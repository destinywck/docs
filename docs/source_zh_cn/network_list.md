# 网络支持

<a href="https://gitee.com/mindspore/docs/tree/master/docs/source_zh_cn/network_list.md" target="_blank"><img src="./_static/logo_source.png"></a>

## Model Zoo
|  领域 | 子领域    | 网络                                   | Ascend | GPU | CPU 
|:------   |:------| :-----------                               |:------   |:------  |:-----
|计算机视觉（CV） | 图像分类（Image Classification）  | [AlexNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/alexnet/src/alexnet.py)          |  Supported |  Supported | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [GoogleNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/googlenet/src/googlenet.py)                                               |  Supported     | Doing | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [LeNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/lenet/src/lenet.py)              |  Supported |  Supported | Supported
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [ResNet-50](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/resnet/src/resnet.py)          |  Supported |  Doing | Doing
|计算机视觉（CV）  | 图像分类（Image Classification）  | [ResNet-101](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/resnet/src/resnet.py)                                              |  Supported |Doing | Doing
| 计算机视觉（CV）  | 图像分类（Image Classification）  | [VGG16](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/vgg16/src/vgg.py)                |  Supported |  Doing | Doing
| 计算机视觉（CV）  | 移动端图像分类（Mobile Image Classification）<br>目标检测（Image Classification）<br>语义分割（Semantic Tegmentation）  | [MobileNetV2](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv2/src/mobilenetV2.py)                                            |  Supported |  Supported | Doing
| 计算机视觉（CV）  | 移动端图像分类（Mobile Image Classification）<br>目标检测（Image Classification）<br>语义分割（Semantic Tegmentation）  | [MobileNetV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/mobilenetv3/src/mobilenetV3.py)                                            |  Doing |  Supported | Doing
|计算机视觉（CV）  | 目标检测（Targets Detection）  | [SSD](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/ssd/src/ssd.py)                   |  Supported |Doing | Doing
| 计算机视觉（CV）  | 目标检测（Targets Detection）  | [YoloV3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/yolov3/src/yolov3.py)         |  Supported |  Doing | Doing
| 计算机视觉（CV）  | 目标检测（Targets Detection）  | [FasterRCNN](https://gitee.com/mindspore/mindspore/tree/master/model_zoo/faster_rcnn/src/FasterRcnn)         |  Supported |  Doing | Doing
| 计算机视觉（CV） | 语义分割（Semantic Segmentation）  | [Deeplabv3](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/deeplabv3/src/deeplabv3.py)                                       |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [BERT](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/bert/src/bert_model.py)                                          |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [Transformer](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/Transformer/src/transformer_model.py)                                          |  Supported |  Doing | Doing
| 自然语言处理（NLP） | 自然语言理解（Natural Language Understanding）  | [SentimentNet](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/lstm/src/lstm.py)                                          |  Doing |  Supported | Supported
| 推荐（Recommender） | 推荐系统、点击率预估（Recommender System, CTR prediction）  | [DeepFM](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/deepfm/src/deepfm.py)                                          |  Supported |  Doing | Doing
| 推荐（Recommender） | 推荐系统、搜索、排序（Recommender System, Search ranking）  | [Wide&Deep](https://gitee.com/mindspore/mindspore/blob/master/model_zoo/wide_and_deep/src/wide_and_deep.py)                                          |  Supported |  Doing | Doing


## 预训练模型
建设中，即将上线
